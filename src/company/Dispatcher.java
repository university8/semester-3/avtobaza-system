package company;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Dispatcher extends Person implements Serializable {
    private List<Trip> trips = new ArrayList<Trip>();
    private List<Driver> drivers = new ArrayList<Driver>();
    private List<Cars> cars = new ArrayList<Cars>();
    private List<Entry> entries = new ArrayList<Entry>();
    private List<Cars> carsOnWay= new ArrayList<Cars>();
    private List<Driver> driversOnWay= new ArrayList<Driver>();
    private List<Driver> suspendedDrivers= new ArrayList<Driver>(); //отстраненные

    public Dispatcher(String name) {
        super(name);
    }
    public List<Driver> getDrivers() { return drivers; }
    public void setDrivers(List<Driver> drivers) { this.drivers = drivers; }
    public void setDrivers(Driver... driver) { this.drivers.addAll(Arrays.asList(driver)); }
    public List<Trip> getTrips() { return trips; }
    public void setTrips(List<Trip> trips) { this.trips = trips; }
    public void setTrips(Trip... trip) { this.trips.addAll(Arrays.asList(trip)); }
    public List<Cars> getCars() { return cars; }
    public void setCars(List<Cars> cars) { this.cars = cars; }
    public void setCars(Cars... car) { this.cars.addAll(Arrays.asList(car)); }

    public  void suspendDriver(Driver driver){
        drivers.remove(driver);
        suspendedDrivers.add(driver);
    }
    public  Entry createEntry(Trip trip,Driver driver,Cars car) {
        if(driversOnWay.contains(driver)){
            throw new IllegalStateException("Водитель " + driver + " уже в дороге!");
        }
        if(suspendedDrivers.contains(driver)){
            throw new IllegalStateException("Водитель " + driver + " уже не работает");
        }
        if(carsOnWay.contains(car)){
            throw new IllegalStateException("Машина " + car + " уже в пути!");
        }
        Entry entry =  new Entry(trip, driver, car);
        entries.add(entry);
        driversOnWay.add(driver);
        carsOnWay.add(car);
        return entry;
    }
    @Override
    public String toString()
    {
        return (super.toString());
    }

}
