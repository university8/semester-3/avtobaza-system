package company;

import java.util.ArrayList;
import java.util.List;
import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        Dispatcher dispatcher1 = new Dispatcher("Igor");
        Dispatcher dispatcher2 = new Dispatcher("Petya");
        Driver driver1 = new Driver("Zhenya");
        Driver driver2 = new Driver("Vasya");
        Driver driver3 = new Driver("Kolya");
        Cars car1 = new Cars("BMW", "1566 BC-7");
        Cars car2 = new Cars("Porsche", "1221 AM-7");
        Cars car3 = new Cars("LADA", "2839 KH-7");
        Cars car4 = new Cars("Toyota", "1892 MF-7");
        Trip trip1 = new Trip("Minsk","Brest");
        Trip trip2 = new Trip("Minsk","Gomel");
        Trip trip3 = new Trip("Minsk","Grodno");
        List<Driver> driverList = new ArrayList<Driver>();
        driverList.add(driver1);
        driverList.add(driver3);
        List<Cars> carList = new ArrayList<Cars>();
        carList.add(car1);
        carList.add(car2);
        List<Trip> tripList = new ArrayList<Trip>();
        tripList.add(trip1);
        tripList.add(trip2);
        Entry entry2 = new Entry();
        dispatcher1.setDrivers(driver1,driver2,driver3);
        Entry  entry1 = dispatcher1.createEntry(trip1, driver1, car1);
        System.out.println(entry1.toString());
        System.out.println("Попытка назначит водителя 1 на вторую заявку...");
        try {
             entry2 = dispatcher1.createEntry(trip2, driver1, car2);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println("Водитель " + driver1 + " завершил рейс");
        driver1.setEntryState(entry1,true);
        System.out.println("Состояние рейса: " + entry1.getCondition());
        System.out.println("Водитель указывает состояние автомобиля и делает " +
                "заявку на ремонт:");
        driver1.setCarState(car1,"Надо отремонтировать");
        driver1.setRepairRequest(car1,true);
        System.out.println("Состояние автомобиля: " + car1.getCarState());
        System.out.println("Нужен ремонт? : " + car1.getRepairNeed());
        System.out.println("Уволим водителя " + driver2);
        dispatcher1.suspendDriver(driver2);
        System.out.println("Попробум назначить уволенного на новый рейс: ");
        try {
            entry2 = dispatcher1.createEntry(trip2, driver2, car2);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        ////////////////////////serialization//////////////////
        System.out.println("Выполняем сериализацию...");
        final String filename_trip = "trips.dat";
        final String filename_cars = "cars.dat";
        final String filename_drivers = "drivers.dat";
        final String filename_dispatchers = "dispatchers.dat";
        final String filename_entry = "entries.dat";
        Connector con_drivers = new Connector(filename_drivers);
        Connector con_cars = new Connector(filename_cars);
        Connector con_trips = new Connector(filename_trip);
        Connector con_entry = new Connector(filename_entry);
        con_drivers.write(driverList);
        con_cars.write(carList);
        con_trips.write(tripList);
        con_entry.write(entry1);
        System.out.println("Выполняем десериализацию...");
        System.out.println("Полученные водители, машины, маршруты: ");
        List<Driver> newDriver = con_drivers.readDrivers();
        for (Driver dr : newDriver) {
                System.out.println(dr);
        }
        List<Cars> newCars = con_cars.readCars();
        for (Cars car : newCars) {
            System.out.println(car);
        }
        List<Trip> newTrips= con_trips.readTrips();
        for (Trip trip : newTrips) {
            System.out.println(trip);
        }
        System.out.println(con_entry.readEntry());
    }
}
