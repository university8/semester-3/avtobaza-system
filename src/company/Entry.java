package company;

import java.io.Serializable;
import java.util.ArrayList;

public class Entry implements Serializable {
    private Trip trip;
    private Driver driver;
    private Cars car;
    boolean isCompleted;
    public Entry(){}
    public Entry(Trip trip,Driver driver,Cars car){
        this.trip = trip;
        this.driver = driver;
        this.car = car;
        isCompleted = false;
    }
    public Trip getEntryTrip() { return trip; }
    public void setEntryTrip(Trip trip) { this.trip = trip; }
    public Driver getEntryDriver() { return driver; }
    public void setEntryDriver(Driver driver) { this.driver = driver; }
    public Cars getEntryCar() { return car; }
    public void setEntryCar(Cars car) { this.car = car; }

    public String getCondition() {
        return isCompleted? "Рейс завершен" : "Рейс в процессе";
    }
    public void setEntryCondition(boolean isCompleted) { this.isCompleted = isCompleted; }
    public String toString() {
        return "Заявка: " + trip + ", " + driver + ", " + car;
    }
}
