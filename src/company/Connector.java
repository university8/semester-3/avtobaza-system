package company;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class Connector {
    private final String filename;
    public Connector(String filename) {
        this.filename = filename;
    }
    public void write(Object... objects) throws IOException {
        try (var out = new ObjectOutputStream(new FileOutputStream(filename))) {
            for (Object obj : objects) {
                out.writeObject(obj);
            }
            System.out.println("Сериализация прошла успешно!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        /////////////////////////close и flush автоматически вызываются благодаря try(){}///////////////////////
    }

    public void write(List<Object> objects) throws IOException {
        try (var out = new ObjectOutputStream(new FileOutputStream(filename))) {
            for (Object obj : objects) {
                out.writeObject(obj);
            }
            System.out.println("Сериализация прошла успешно!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Driver> readDrivers(){
        List<Driver> driverList = null;
        try (var in = new ObjectInputStream(new FileInputStream(filename))) {
            driverList = (ArrayList)in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return driverList;
    }
    public Driver readDriver(){
        Driver driver = null;
        try (var in = new ObjectInputStream(new FileInputStream(filename))) {
            driver = (Driver)in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return driver;
    }
    public List<Trip> readTrips(){
        List<Trip> tripList = null;
        try (var in = new ObjectInputStream(new FileInputStream(filename))) {
            tripList = (ArrayList)in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return tripList;
    }
    public Trip readTrip(){
        Trip trip = null;
        try (var in = new ObjectInputStream(new FileInputStream(filename))) {
            trip = (Trip)in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return trip;
    }
    public List<Cars> readCars(){
        List<Cars> carsList = null;
        try (var in = new ObjectInputStream(new FileInputStream(filename))) {
            carsList = (ArrayList)in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return carsList;
    }
    public Cars readCar(){
        Cars car= null;
        try (var in = new ObjectInputStream(new FileInputStream(filename))) {
            car = (Cars)in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return car;
    }
    public List<Dispatcher> readDispatchers(){
        List<Dispatcher> dispatcherListi = null;
        try (var in = new ObjectInputStream(new FileInputStream(filename))) {
            dispatcherListi = (ArrayList)in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return dispatcherListi;
    }
    public Dispatcher readDispatcher(){
        Dispatcher dispatcher= null;
        try (var in = new ObjectInputStream(new FileInputStream(filename))) {
            dispatcher = (Dispatcher)in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return dispatcher;
    }
    public List<Entry> readEntries(){
        List<Entry> entryList = null;
        try (var in = new ObjectInputStream(new FileInputStream(filename))) {
            entryList = (ArrayList)in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return entryList;
    }
    public Entry readEntry(){
        Entry entry1 = null;
        try (var in = new ObjectInputStream(new FileInputStream(filename))) {
            entry1 = (Entry)in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return entry1;
    }
//
}