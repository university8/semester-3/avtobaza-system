package company;

import java.io.Serializable;

public class Trip implements Serializable {
    private String start;
    private String finish;

    public Trip(){};
    public Trip(String start,String finish){
        this.start = start;
        this.finish = finish;
    };
    public String getStart() { return start; }
    public void setStart(String finish) { this.start = start; }
    public String getFinish() { return finish; }
    public void setFinish(String finish) { this.finish = finish; }

    public String toString() {
        return "Маршрут: " + start + " - " + finish;
    }

}
