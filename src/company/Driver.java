package company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Driver extends Person {

    public Driver(String name) {
        super(name);
    }

    public void setRepairRequest(Cars car, boolean repairRequest) {
        car.setRepairNeed(repairRequest);
    }
    public void setEntryState(Entry entry,boolean isCompleted) {
        entry.setEntryCondition(isCompleted);
    }
    public void setCarState(Cars car,String carState) { car.setCarState(carState); }
    @Override
    public String toString()
    {
        return (super.toString());
    }
}
