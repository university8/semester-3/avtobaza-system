package company;

import java.io.Serializable;

public class Cars implements Serializable {
    private String brand;
    private String carNumber;
    String carState;
   private boolean needRepair;
    public Cars(String brand,String carNumber)
    {
       this.brand = brand;
        this.carNumber = carNumber;
    }
    public String getBrand() { return brand; }
    public void setBrand(String brand) { this.brand = brand; }
    public String getCarNumber() { return carNumber; }
    public void setCarNumber(String carNumber) { this.carNumber = carNumber; }
    public void setRepairNeed(boolean needRepair) { this.needRepair = needRepair; }
    public String getRepairNeed() {   return needRepair? "Да" : "Нет"; }
    public void setCarState(String carState) { this.carState = carState; }
    public String getCarState() { return carState; }
    public String toString() {
        return "Машина: " + brand + ", " + carNumber;
    }
}
